package com.twuc.webApp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiplicationTableController {

    @RequestMapping("/api/tables/multiply")
    public String MultiplicationTable() {
        return getMultiplicationTable();
    }

    public String getMultiplicationTable(){
        StringBuilder sb = new StringBuilder();
        for(int i=1;i<10;i++){
            for(int j=1;j<=i;j++){
                String tmp = String.format("%d*%d=%-3d",i,j,i*j);
                sb.append(tmp);
            }
            sb.append("\n");
        }
        return sb.toString();
    }


}
