package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AdditionTableController {

    @RequestMapping("/api/tables/plus")
    public String AdditionTable() {
        return getAdditionTable();
    }

    public String getAdditionTable(){
        StringBuilder sb = new StringBuilder();
        for(int i=1;i<10;i++){
            for(int j=1;j<=i;j++){
                String tmp = String.format("%d+%d=%-3d",i,j,i+j);
                sb.append(tmp);
            }
            sb.append("\n");
        }
        return sb.toString();
    }


}
